package edu.rpi.rair;

/**
 * Created by luca on 1/20/17.
 */
public interface PlannerListener {
  public void droppedGoal(Goal goal, Goal inFavorOf);
}
